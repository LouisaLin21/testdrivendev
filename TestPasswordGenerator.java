import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class TestPasswordGenerator {
    public static void main(String[] args) {

    }

    @Test
    public void testGenerateWeakpassword() {

        PasswordGenerator p1 = new PasswordGenerator(5);
        assertEquals("asdfasds", p1.getGenerateWeakPassword());
    }

    @Test
    public void testGenerateStrongPassowrd() {
        PasswordGenerator p2 = new PasswordGenerator(6);
        assertEquals("Apple", p2.getGenerateStrongPassword());
    }

}