import java.util.*;
public class PasswordGenerator {
    private int passwordLength;
    public PasswordGenerator(int n){
        this.passwordLength=n;
    }
    public String generateWeakPassowrd(){
        String resultPassword="";
        String weak ="abcdefghijklmnopqrstuvwxyz";
        Random r = new Random();
        for (int i=0;i<this.passwordLength;i++){
            int ranIndex= r.nextInt(26);    
            resultPassword +=weak.charAt(ranIndex);
        }
        return resultPassword;
    }
    public String generateStrongPassword(){
        String resultPassword="";
        String weak ="abcdefghijklmnopqrstuvwxyz";
        Random r = new Random();
        for (int i=0;i<this.passwordLength*2;i++){
            int ranIndex= r.nextInt(26);    
            resultPassword +=weak.charAt(ranIndex);
        }
        return resultPassword;
    }
}
