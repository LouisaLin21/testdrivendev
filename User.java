public class User {
    private String userName;
    private String passWord;
    private String phoneNumber;
    private UserType type;

    public User(String u, String p,String phone,UserType t){
        this.userName=u;
        this.phoneNumber=phone;
        this.passWord=p;
        this.type=t;
    }
    public String getUsername(){
        return this.userName;
    }
    public String getPassword(){
        return this.passWord;
    }
    public String getPhoneNumber(){
        return this.phoneNumber;
    }
    public UserType getUserType(){
        return this.type;
    }
}
