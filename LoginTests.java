import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class LoginTests {
    //This method tests the validation of the username and password
    public void testValidate(){
    //1.username is valid, but the passoword or the phone number is not
    //2.username and password are both valid
    //3.username and password are valid but the type is admin
    //4. username and password are both invalid
    User[] u = new User[3];
    u[0]=new User("l1","1234","514719",UserType.REGULAR);
    u[1]=new User("l2","4567","514729",UserType.ADMIN);
    u[2]=new User("l3","1234","514739",UserType.REGULAR);
    
    assertEquals(true, Login.validate("l1","1234","514718",UserType.REGULAR));
    assertEquals(false, Login.validate("l1","1234","514719",UserType.ADMIN));
    assertEquals(true, Login.validate("l2","4567","514729",UserType.REGULAR));
    assertEquals(true, Login.validate("l1","1234","514718",UserType.REGULAR));
    }
}
