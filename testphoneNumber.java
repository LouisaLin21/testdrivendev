import jdk.jfr.Timestamp;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class testphoneNumber{
    public static void main (String arg){

    }

    @Test 
    public void testvalodate(){
        PhoneNumberValidator a = new PhoneNumberValidator("1234456");
        PhoneNumberValidator b = new PhoneNumberValidator("123sjxnbs456");
        // phone number is 123456 no character included
        assertTrue(a.validate() == true);
        //phone number is 123sjxnbs456, include character
        assertTrue(b.validate() == false);
    }
}