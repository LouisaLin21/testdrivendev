public class PhoneNumberValidator {
    private String phoneNumber;

    /*
     * constructor that accepts as input a boolean and stores whether brackets ( )
     * are allowed in the number or not
     */
    public PhoneNumberValidator(String phoneNumber, boolean allowBracket) {
        if (phoneNumber.length() != 10) {
            throw new IllegalArgumentException("phone number has to be 10 digits");
        }

        if (allowBracket) {
            String numberBracket = "(";
            for (int i = 0; i < 3; i++) {
                numberBracket += phoneNumber.charAt(i);
            }
            numberBracket += ")";
            for (int i = 3; i < phoneNumber.length(); i++) {
                numberBracket += phoneNumber.charAt(i);
            }
            this.phoneNumber = numberBracket;
        } else {
            this.phoneNumber = phoneNumber;
        }
    }

    /*
     * method validate(String number): This method should return true if the phone
     * number given is valid and false otherwise. A phone number is valid if it
     * contains only numerical numbers . A phone number is valid if it contains only
     * numerical numbers (Hint: Look inside the Character class for some library
     * methods to help!) Depending on the boolean value it is also allowed to store
     * ( ) inside of it (You do not need to check if they match though!!)
     */
    public boolean Validate() {
        if (this.phoneNumber.charAt(0) == 40 && this.phoneNumber.charAt(4) == 41) {
            String newNumber = removePhoneNumberBracket();
            return checkDigit(newNumber);
        } else {
            return checkDigit(this.phoneNumber);
        }
    }

    /*
     * help method to remove () of the phoneNumber return a new string of Phone
     * number with no bracket
     */
    public String removePhoneNumberBracket() {
        String noBracketPhone = "";
        for (int i = 1; i < 4; i++) {
            noBracketPhone += this.phoneNumber.charAt(i);
        }
        for (int i = 5; i < this.phoneNumber.length(); i++) {
            noBracketPhone += this.phoneNumber.charAt(i);
        }
        return noBracketPhone;
    }

    /*
     * check phoneNumber if it contains only numerical numbers take input of phone
     * Number as string and use charAt() and Character.isDigit() to verify and
     * return boolean
     */
    public boolean checkDigit(String phoneNumber) {
        for (int i = 0; i < this.phoneNumber.length(); i++) {
            if (Character.isDigit(this.phoneNumber.charAt(i)) == false) {
                return false;
            }
        }
        return true;
    }
}
